Mypasswords CLI
===============

CLI for your passwords API.


Install
-------

This python project requires `python 3.6`, otherwise the `pip install` should
work fine:

.. code:: bash

    $ pip install mypasswords-cli


Usage
-----

By default we can run the server as `python` module as follow:

.. code:: bash

    $ mypasswords-cli
    usage: mypasswords-cli [-h] [-a BASIC_AUTH] [-k] [-u URL]
                      {add,delete,search,show,update} ...

    positional arguments:
      {add,delete,search,show,update}

    optional arguments:
      -h, --help            show this help message and exit
      -a BASIC_AUTH, --basic_auth BASIC_AUTH
      -k, --insecure
      -u URL, --url URL
      --client-key CLIENT_KEY
      --server-crt SERVER_CRT


API
---

Add
^^^

.. code:: bash

    $ mypasswords-cli add gitlab.com -l toopy
    Please enter a password to set:
    Password: 

    ┌──────────────────────────────────────┬───────┬────────────┐
    │ id                                   │ login │ site       │
    ├──────────────────────────────────────┼───────┼────────────┤
    │ ce35a5f1-6f83-48bc-b8b3-6f8e6021ae42 │ toopy │ gitlab.com │
    └──────────────────────────────────────┴───────┴────────────┘


Update
^^^^^^

.. code:: bash

    $ mypasswords-cli update gitlab.com -l toopy
    Please enter a password to set:
    Password: 
    ┌──────────────────────────────────────┬───────┬────────────┐
    │ id                                   │ login │ site       │
    ├──────────────────────────────────────┼───────┼────────────┤
    │ ce35a5f1-6f83-48bc-b8b3-6f8e6021ae42 │ toopy │ gitlab.com │
    └──────────────────────────────────────┴───────┴────────────┘


Search
^^^^^^

.. code:: bash

   $ mypasswords-cli search too
    ┌──────────────────────────────────────┬───────┬────────────┐
    │ id                                   │ login │ site       │
    ├──────────────────────────────────────┼───────┼────────────┤
    │ ce35a5f1-6f83-48bc-b8b3-6f8e6021ae42 │ toopy │ gitlab.com │
    └──────────────────────────────────────┴───────┴────────────┘


Show
^^^^

.. code:: bash

    $ mypasswords-cli show gitlab.com
    ┌──────────────────────────────────────┬───────┬──────────┬────────────┐
    │ id                                   │ login │ password │ site       │
    ├──────────────────────────────────────┼───────┼──────────┼────────────┤
    │ ce35a5f1-6f83-48bc-b8b3-6f8e6021ae42 │ toopy │ ******   │ gitlab.com │
    └──────────────────────────────────────┴───────┴──────────┴────────────┘


Delete
^^^^^^

.. code:: bash

  $ mypasswords-cli delete gitlab.com


License
-------

MIT License
