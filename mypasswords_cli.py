import argparse
import os
import sys
from getpass import getpass
from typing import Any, Dict, List, Tuple

import jwt
import urllib3
from cached_property import cached_property
from requests import request
from requests.auth import HTTPBasicAuth
from requests.exceptions import RequestException
from terminaltables import SingleTable

urllib3.disable_warnings()

COMMON_OPTIONS = [
    (("-a", "--basic_auth",), dict(default=os.getenv("MYPASSWORDS_BASIC_AUTH"),),),
    (
        ("-d", "--debug",),
        dict(
            action="store_true",
            default=os.getenv("MYPASSWORDS_DEBUG") == "true" or False,
        ),
    ),
    (
        ("-k", "--insecure",),
        dict(
            action="store_true",
            default=os.getenv("MYPASSWORDS_INSECURE") == "true" or False,
        ),
    ),
    (
        ("-u", "--url",),
        dict(default=os.getenv("MYPASSWORDS_URL") or "http://127.0.0.1:8000",),
    ),
    (("--client-key",), dict(default=os.getenv("MYPASSWORDS_CLIENT_KEY")),),
    (("--server-crt",), dict(default=os.getenv("MYPASSWORDS_SERVER_CRT")),),
]


DEFAULT_SUBOPTIONS = [
    (["site",], {}),
]  # type: List[Tuple[List[str], Dict[Any, Any]]]


ADD_UPDATE_SUBOPTIONS = DEFAULT_SUBOPTIONS + [
    (["-l", "--login",], {}),
    (["--new_site",], {}),
]


METHOD_BINDING = {
    "add": "POST",
    "delete": "DELETE",
    "update": "PUT",
}


class AlreadyExist(Exception):
    pass


class AmbiguousSite(Exception):
    pass


class NotFound(Exception):
    pass


class Cli:
    @cached_property
    def password(self):
        return getpass()

    @cached_property
    def data(self):
        command = self.settings.subparsers
        if command not in ["add", "update"]:
            return

        data = {
            "password": self.password,
            "site": self.settings.new_site or self.settings.site,
        }

        if self.settings.login:
            data["login"] = self.settings.login

        return {"jwt": jwt.encode(data, self.client_key, algorithm="RS256").decode()}

    @cached_property
    def id_(self):
        command = self.settings.subparsers
        if command == "search":
            return

        params = {"q": self.settings.site}
        data = self.perform_request(params=params, lookup=True)

        if command == "add" and not data:
            return
        elif command == "add" and len(data) != 0:
            raise AlreadyExist
        elif command in ["delete", "update", "show"] and len(data) != 1:
            raise NotFound if not data else AmbiguousSite

        return data[0]["id"]

    @cached_property
    def client_key(self):
        path = self.settings.client_key
        if not path or not os.path.exists(path):
            sys.stderr.write("Client key priv not found: {}\n".format(path))
            sys.exit(1)
        with open(path) as f:
            return f.read()

    @cached_property
    def server_crt(self):
        path = self.settings.server_crt
        if not path or not os.path.exists(path):
            sys.stderr.write("Server key pub not found: {}\n".format(path))
            sys.exit(1)
        with open(path) as f:
            return f.read()

    @cached_property
    def method(self):
        return METHOD_BINDING.get(self.settings.subparsers, "GET")

    @cached_property
    def passwords(self):
        command = self.settings.subparsers
        params = {} if command != "search" else {"q": self.settings.site}

        passwords = self.perform_request(params=params) or []

        if not isinstance(passwords, list):
            passwords = [passwords]

        if command != "show":
            passwords = [
                {k: v for k, v in d.items() if k != "password"} for d in passwords
            ]

        return passwords

    @cached_property
    def settings(self):
        parser = argparse.ArgumentParser()

        for a, kw in COMMON_OPTIONS:
            parser.add_argument(*a, **kw)

        subparsers = parser.add_subparsers(dest="subparsers")

        parser_add = subparsers.add_parser("add")
        for a, kw in ADD_UPDATE_SUBOPTIONS:
            parser_add.add_argument(*a, **kw)

        parser_delete = subparsers.add_parser("delete")
        for a, kw in DEFAULT_SUBOPTIONS:
            parser_delete.add_argument(*a, **kw)

        parser_search = subparsers.add_parser("search")
        for a, kw in DEFAULT_SUBOPTIONS:
            parser_search.add_argument(*a, **kw)

        parser_show = subparsers.add_parser("show")
        for a, kw in DEFAULT_SUBOPTIONS:
            parser_show.add_argument(*a, **kw)

        parser_update = subparsers.add_parser("update")
        for a, kw in ADD_UPDATE_SUBOPTIONS:
            parser_update.add_argument(*a, **kw)

        settings, _ = parser.parse_known_args(sys.argv[1:])

        if settings.basic_auth and len(settings.basic_auth.split(":")) == 1:
            settings.password += ":{}".format(getpass())

        if not settings.subparsers:
            parser.print_help()
            sys.exit(0)

        return settings

    def perform_request(self, params=None, lookup=False):
        method = "GET" if lookup else self.method
        id_ = None if lookup else self.id_
        data = None if lookup else self.data

        auth = (
            None
            if not self.settings.basic_auth
            else HTTPBasicAuth(*self.settings.basic_auth.split(":"))
        )

        url = "{}/{}".format(self.settings.url, "passwords")
        if id_:
            url += "/{}".format(id_)

        response = request(
            method,
            url,
            auth=auth,
            json=data,
            params=params,
            verify=not self.settings.insecure,
        )

        try:
            response.raise_for_status()
        except RequestException as err:
            resp = err.response
            if resp.status_code in ["401", "403"]:
                sys.stderr.write("Unauthorized\n")
            elif resp.status_code < 500:
                sys.stderr.write("Invalid request\n")
            else:
                sys.stderr.write("Server error\n")

            if self.settings.debug:
                sys.stderr.write("{}\n".format(resp.content.decode()))

            sys.exit(1)

        if method == "DELETE":
            return None
        elif method == "GET" and id_:
            return jwt.decode(
                response.json()["jwt"], self.server_crt, algorithms="RS256"
            )
        else:
            return response.json()


def main():
    try:
        passwords = Cli().passwords
    except AlreadyExist:
        sys.stdout.write("Already exist\n")
        sys.exit(1)
    except AmbiguousSite:
        sys.stdout.write("Ambiguous site\n")
        sys.exit(1)
    except NotFound:
        sys.stdout.write("Not found\n")
        sys.exit(1)
    except KeyboardInterrupt:
        sys.stdout.write("\n")
        return

    if not passwords:
        return

    heads = [c for c in sorted(passwords[0].keys())]
    lines = [[p[c] for c in heads] for p in passwords]

    table = [heads]
    table += lines

    sys.stdout.write(SingleTable(table).table)
    sys.stdout.write("\n")


if __name__ == "__main__":
    main()
