import json
from collections import namedtuple

import jwt
import pytest
import responses
from mypasswords_cli import AlreadyExist, Cli


@pytest.fixture
def server_key():
    with open("data/server.key") as f:
        return f.read()


@pytest.fixture
def settings_mock(mocker):
    def _settings_mocker(**kw):
        settings = namedtuple("Settings", " ".join(kw.keys()))(**kw)
        mocker.patch(
            "mypasswords_cli.Cli.settings",
            new_callable=mocker.PropertyMock,
            return_value=settings,
        )

    return _settings_mocker


def test_data(mocker, settings_mock):
    mocker.patch(
        "mypasswords_cli.Cli.password",
        new_callable=mocker.PropertyMock,
        return_value="xxx",
    )
    settings_mock(
        client_key="data/client.key",
        login=None,
        new_site=None,
        subparsers="add",
        site="foo.io",
    )
    cli = Cli()
    with open("data/client.crt") as f:
        assert jwt.decode(cli.data["jwt"], f.read(), algorithms="RS256") == {
            "password": "xxx",
            "site": "foo.io",
        }


def test_client_key(settings_mock):
    settings_mock(client_key="data/client.key")
    assert Cli().client_key.startswith("-----BEGIN RSA PRIVATE KEY")


def test_server_crt(settings_mock):
    settings_mock(server_crt="data/server.crt")
    assert Cli().server_crt.startswith("-----BEGIN PUBLIC KEY")


def test_perform_request_list(settings_mock):
    settings_mock(
        basic_auth="", insecure=True, subparsers="search", url="http://localhost",
    )
    with responses.RequestsMock() as r:
        r.add(
            responses.GET,
            "http://localhost/passwords",
            body='[{"foo": "bar"}]',
            status=200,
            content_type="application/json",
        )
        assert Cli().perform_request(params={"q": "*"}) == [{"foo": "bar"}]


def test_perform_request_get(mocker, server_key, settings_mock):
    mocker.patch(
        "mypasswords_cli.Cli.id_", new_callable=mocker.PropertyMock, return_value="foo"
    )
    settings_mock(
        basic_auth="",
        insecure=True,
        site="a-site",
        server_crt="data/server.crt",
        subparsers="show",
        url="http://localhost",
    )
    with responses.RequestsMock() as r:
        r.add(
            responses.GET,
            "http://localhost/passwords/foo",
            body=json.dumps(
                {
                    "jwt": jwt.encode(
                        {"foo": "bar"}, server_key, algorithm="RS256"
                    ).decode()
                }
            ),
            status=200,
            content_type="application/json",
        )
        assert Cli().perform_request() == {"foo": "bar"}


def test_perform_request_add(mocker, settings_mock):
    mocker.patch(
        "mypasswords_cli.Cli.password",
        new_callable=mocker.PropertyMock,
        return_value="xxx",
    )
    data = {
        "login": "foo",
        "site": "a-site",
    }
    settings_mock(
        basic_auth="",
        insecure=True,
        client_key="data/client.key",
        new_site=None,
        subparsers="add",
        url="http://localhost",
        **data
    )
    with responses.RequestsMock() as r:
        r.add(
            responses.GET,
            "http://localhost/passwords",
            body="[]",
            status=200,
            content_type="application/json",
        )
        r.add(
            responses.POST,
            "http://localhost/passwords",
            body=json.dumps(data),
            status=200,
            content_type="application/json",
        )
        assert Cli().perform_request() == {"login": "foo", "site": "a-site"}


def test_perform_request_add_conflict(mocker, settings_mock):
    mocker.patch(
        "mypasswords_cli.Cli.password",
        new_callable=mocker.PropertyMock,
        return_value="xxx",
    )
    data = {
        "login": "foo",
        "site": "a-site",
    }
    settings_mock(
        basic_auth="",
        insecure=True,
        client_key="data/client.key",
        new_site=None,
        subparsers="add",
        url="http://localhost",
        **data
    )
    with responses.RequestsMock() as r:
        r.add(
            responses.GET,
            "http://localhost/passwords",
            body='["found"]',
            status=200,
            content_type="application/json",
        )
        with pytest.raises(AlreadyExist):
            assert Cli().perform_request()


def test_perform_request_update(mocker, settings_mock):
    mocker.patch(
        "mypasswords_cli.Cli.id_", new_callable=mocker.PropertyMock, return_value="foo"
    )
    mocker.patch(
        "mypasswords_cli.Cli.password",
        new_callable=mocker.PropertyMock,
        return_value="xxx",
    )
    data = {
        "site": "a-site",
    }
    settings_mock(
        basic_auth="",
        insecure=True,
        client_key="data/client.key",
        login=None,
        new_site=None,
        subparsers="update",
        url="http://localhost",
        **data
    )
    with responses.RequestsMock() as r:
        r.add(
            responses.PUT,
            "http://localhost/passwords/foo",
            body=json.dumps(data),
            status=200,
            content_type="application/json",
        )
        assert Cli().perform_request() == {"site": "a-site"}


def test_perform_request_delete(mocker, settings_mock):
    mocker.patch(
        "mypasswords_cli.Cli.id_", new_callable=mocker.PropertyMock, return_value="foo"
    )
    settings_mock(
        basic_auth="", insecure=True, subparsers="delete", url="http://localhost",
    )
    with responses.RequestsMock() as r:
        r.add(
            responses.DELETE,
            "http://localhost/passwords/foo",
            body="",
            status=204,
            content_type="application/json",
        )
        assert not Cli().perform_request()
